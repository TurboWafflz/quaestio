from flask import (
	Flask,
	request,
	session,
	redirect,
	url_for,
	render_template,
	send_file
)
from flask_apscheduler import APScheduler
import quaestio
import time
import random
import urllib.parse
import uuid
import multiprocessing
import os
import re

app = Flask(__name__)
scheduler=APScheduler()
scheduler.init_app(app)
scheduler.start()

quizzes={}

## Clean up old quizzes
def cleanup():
	oldQuizzes=[]
	print("Cleaning up old quizzes...")
	for quiz in quizzes:
		if time.time() >= quizzes[quiz]["expiration"]:
			oldQuizzes.append(quiz)
	for quiz in oldQuizzes:
			print(f"Deleted {quiz}")
			del quizzes[quiz]
scheduler.add_job(id="cleanup-job", func=cleanup, trigger='interval', seconds=300)

@app.route('/')
def quizSelect():
	examples=os.listdir("exampleSets")
	return render_template("menu.html", examples=examples)

@app.route('/quiz/edit/<quizFile>')
def editQuiz(quizFile):
	if quizFile=="custom":
		if request.args.get("set") != None:
			template=request.args.get("set")
		else:
			template=""
	else:
		template=open(f"exampleSets/{quizFile}").read()
	return render_template("editor.html", template=template.replace("\n", "\\n"))

@app.route('/quiz/generate')
def generateQuiz():
	## Get set from URL parameter if available
	rawSet=request.args.get('set')
	if not rawSet == None:
		set=urllib.parse.unquote(rawSet)
		## Load question set
		quiz=quaestio.quiz()
		try:
			quiz.loadString(set)
		except:
			return "Invalid question set, make sure every question has an answer"

		## Generate quiz
		if not request.args.get("answerType") == None:
			answer=request.args.get("answerType")
		else:
			answer="term"
		questions, answers = quiz.multiChoice(answer=answer)

		## Generate quiz ID
		quizID=hex(hash(str(questions))*hash(request.remote_addr)*hash(time.time()))

		if len(questions) == 0:
			return("You must add at least one term")
		## Put quiz into dictionary
		quizzes[quizID]={}
		quizzes[quizID]["questions"]=questions
		quizzes[quizID]["answers"]=answers
		quizzes[quizID]["questionNumber"]=0
		quizzes[quizID]["correctAnswers"]=0
		quizzes[quizID]["expiration"]=time.time()+86400000

		## Redirect user to generated quiz
		return redirect(f"/quiz/take/{quizID}", 302)

@app.route('/quiz/<quizFile>')
def loadQuiz(quizFile):
	global quizzes

	## Load question set
	quiz=quaestio.quiz()
	try:
		quiz.load(f"exampleSets/{quizFile}")
	except:
		return "Unable to load quiz"

	## Generate quiz
	questions, answers = quiz.multiChoice()

	## Generate quiz ID
	quizID=hex(hash(str(questions))*hash(request.remote_addr)*hash(time.time()))

	## Put quiz into dictionary
	quizzes[quizID]={}
	quizzes[quizID]["questions"]=questions
	quizzes[quizID]["answers"]=answers
	quizzes[quizID]["questionNumber"]=0
	quizzes[quizID]["correctAnswers"]=0

	## Redirect user to generated quiz
	return redirect(f"/quiz/take/{quizID}")

@app.route('/quiz/take/<quizID>')
def displayQuiz(quizID):
	global quizzes

	## Make sure URL is valid
	if not quizID in quizzes:
		return "Quiz ID no longer valid"

	## Show results once end of quiz is reached
	if quizzes[quizID]["questionNumber"] == len(list(quizzes[quizID]["questions"])):
		total=quizzes[quizID]["questionNumber"]
		correct=quizzes[quizID]["correctAnswers"]
		percent=f"{str(round(quizzes[quizID]['correctAnswers']/quizzes[quizID]['questionNumber']*100))}%"
		del quizzes[quizID]
		return render_template("quiz/end.html", total=total, correct=correct, percent=percent)

	questionName=list(quizzes[quizID]["questions"])[quizzes[quizID]["questionNumber"]]

	## Check answer if there is an answer URL parameter
	answer=request.args.get('answer')
	if not answer==None:
		answer=urllib.parse.unquote(answer)
		if answer==quizzes[quizID]["answers"][questionName]:
			quizzes[quizID]["questionNumber"]+=1
			quizzes[quizID]["correctAnswers"]+=1
			return render_template("quiz/answer.html", status="Correct", correctAnswer=quizzes[quizID]["answers"][questionName], answer=answer)
		else:
			quizzes[quizID]["questionNumber"]+=1
			return render_template("quiz/answer.html", status="Incorrect", correctAnswer=quizzes[quizID]["answers"][questionName], answer=answer)

	## Render question if no answer parameter
	question=quizzes[quizID]["questions"][questionName]
	answer=quizzes[quizID]["answers"][questionName]
	return render_template("quiz/question.html", question=question, answer=answer, questionName=questionName, questionNumber=quizzes[quizID]["questionNumber"]+1, total=len(list(quizzes[quizID]["questions"])))

@app.route("/favicon.ico")
def favicon():
	return send_file("favicon.ico")
