import colorama
import random
from copy import deepcopy
import os
colorama.init(autoreset=True)

def shuffleList(lst):
	temp_lst = deepcopy(lst)
	m = len(temp_lst)
	while (m):
		m -= 1
		i = random.randint(0, m)
		temp_lst[m], temp_lst[i] = temp_lst[i], temp_lst[m]
	return temp_lst



class quiz():
	def __init__(self):
		self.terms={}
	def load(self, file):
		## Load terms/definititions from file (compatible with default Quizlet export format)
		with open(file, "r") as termFile:
			termsRaw=termFile.readlines()
			for term in termsRaw:
				## Ignore empty lines
				if term=="":
					continue
				parts=term.split("	")
				self.terms[parts[0]]=parts[1].rstrip()
	def loadString(self, string):
		termsRaw=string.split("\n")
		for term in termsRaw:
			## Ignore empty lines
			if term=="":
				continue
			parts=term.split("	")
			self.terms[parts[0]]=parts[1].rstrip()
	def selectQuestions(self, questions=None):
		## Pick specified number of random questions with no duplicates
		if questions==None:
			questions=len(self.terms)
		questions=shuffleList(list(self.terms))[:questions]
		return questions
	def multiChoice(self, questions=None, answer="term", answers=4):
		## Pick questions
		questions=self.selectQuestions(questions=questions)
		questionsAnswers={}
		correctAnswers={}
		for question in questions:
			if answer=="term":
				q=self.terms[question]
				a=[]
				a.append(question)
				correctAnswers[self.terms[question]]=question

				## Pick incorrect answers with no duplicates
				for i in range(answers-1):
					possibleQuestions=list(self.terms)
					for alreadyUsed in a:
						possibleQuestions.remove(alreadyUsed)
					if len(possibleQuestions)==0:
						## Not enough terms to generate requested number of answers
						break
					a.append(random.choice(possibleQuestions))
			elif answer=="definition":
				q=question
				a=[]
				a.append(self.terms[question])
				correctAnswers[q]=self.terms[question]

				## Pick incorrect answers with no duplicates
				for i in range(answers-1):
					possibleQuestions=list(self.terms)
					for alreadyUsed in a:
						possibleQuestions.remove(next(key for key, value in self.terms.items() if value == alreadyUsed))
					if len(possibleQuestions)==0:
						## Not enough terms to generate requested number of answers
						break
					aTerm=random.choice(possibleQuestions)
					a.append(self.terms[aTerm])

			## Shuffle answers
			a=shuffleList(a)

			questionsAnswers[q]=a
		return(questionsAnswers, correctAnswers)
	def quiz(self):
		## Generate questions and answers
		questionsAnswers, correctAnswers=self.multiChoice()
		totalQuestions=len(questionsAnswers)
		doneQuestions=0
		correctQuestions=0

		## Iterate through generated questions
		for question in questionsAnswers:
			os.system("clear")
			print(f"{doneQuestions+1}/{totalQuestions}: {question}")

			## Print answers
			for i in range(len(questionsAnswers[question])):
				print(f"{i+1}. {questionsAnswers[question][i]}")

			## Allow user to select answer and verify it is valid
			while True:
				choice=input(": ")
				try:
					choice=int(choice)-1
					selectedAnswer=questionsAnswers[question][choice]
					break
				except:
					print("Invalid selection")
			## Check answer
			correctAnswer=correctAnswers[question]
			os.system("clear")
			doneQuestions+=1
			if selectedAnswer==correctAnswer:
				print(colorama.Fore.GREEN + "Correct")
				print(correctAnswer)
				correctQuestions+=1
			else:
				print(colorama.Fore.RED + "Incorrect")
				print(f"Your answer: {selectedAnswer}")
				print(f"Correct answer: {correctAnswer}")
			input("[Press enter to continue]")
		print(f"Score: {correctQuestions}/{totalQuestions} ({round(correctQuestions/totalQuestions*100)}%)")
